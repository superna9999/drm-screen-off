#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <xf86drm.h>
#include <xf86drmMode.h>
#include <sys/time.h>
#include <getopt.h>

struct drm_dev {
	int fd;
	uint32_t conn_id, enc_id, crtc_id, plane_id, crtc_idx;
	drmModeCrtc *saved_crtc;
	drmModeAtomicReq *req;
	uint32_t count_props;
	drmModePropertyPtr props[128];
	struct drm_dev *next;
};

static struct drm_dev *pdev;

#define DBG_TAG "  drm"

#define print(msg, ...)							\
	do {								\
			struct timeval tv;				\
			gettimeofday(&tv, NULL);			\
			fprintf(stderr, "%08u:%08u :" msg,		\
				(uint32_t)tv.tv_sec,			\
				(uint32_t)tv.tv_usec, ##__VA_ARGS__);	\
	} while (0)

#define err(msg, ...)  print("error: " msg "\n", ##__VA_ARGS__)
#define info(msg, ...) print(msg "\n", ##__VA_ARGS__)
#define dbg(msg, ...)  print(DBG_TAG ": " msg "\n", ##__VA_ARGS__)

uint32_t get_property_id(const char *name)
{
	uint32_t i;

	for (i = 0; i < pdev->count_props; ++i)
		if (!strcmp(pdev->props[i]->name, name))
			return pdev->props[i]->prop_id;

	return 0;
} 

int drm_get_crtc_props(int fd, uint32_t id)
{
	uint32_t i;

	drmModeObjectPropertiesPtr props = drmModeObjectGetProperties(fd, id, DRM_MODE_OBJECT_CRTC);
	if (!props) {
		err("drmModeObjectGetProperties failed\n");
		return -1;
	}
	printf("Found %u props\n", props->count_props);
	pdev->count_props = props->count_props;
	for (i = 0; i < props->count_props; i++) {
		pdev->props[i] = drmModeGetProperty(fd, props->props[i]);
		print("Added prop %u:%s\n", pdev->props[i]->prop_id, pdev->props[i]->name);
	}
	drmModeFreeObjectProperties(props);

	return 0;
}

int drm_get_plane_props(int fd, uint32_t id)
{
	uint32_t i;

	drmModeObjectPropertiesPtr props = drmModeObjectGetProperties(fd, id, DRM_MODE_OBJECT_PLANE);
	if (!props) {
		err("drmModeObjectGetProperties failed\n");
		return -1;
	}
	printf("Found %u props\n", props->count_props);
	pdev->count_props = props->count_props;
	for (i = 0; i < props->count_props; i++) {
		pdev->props[i] = drmModeGetProperty(fd, props->props[i]);
		print("Added prop %u:%s\n", pdev->props[i]->prop_id, pdev->props[i]->name);
	}
	drmModeFreeObjectProperties(props);

	return 0;
}

void drm_free_props(void)
{
	uint32_t i;

	for (i = 0; i < pdev->count_props; i++)
		drmModeFreeProperty(pdev->props[i]);

	pdev->count_props = 0;
}

int drm_add_property(unsigned int id, const char *name, uint64_t value)
{
	int ret;
	uint32_t prop_id = get_property_id(name);

	if (!prop_id) {
		err("Couldn't find prop %s\n", name);
		return -1;
	}

	ret = drmModeAtomicAddProperty(pdev->req, id, prop_id, value);
	if (ret < 0) {
		err("drmModeAtomicAddProperty (%s:%lu) failed: %d\n",
		    name, value, ret);
		return ret;
	}

	return 0;
}

static int find_planes(int fd, uint32_t crtc_id, uint32_t crtc_idx)
{
	drmModePlaneResPtr planes;
	drmModePlanePtr plane;
	unsigned int i;

	planes = drmModeGetPlaneResources(fd);
	if (!planes) {
		err("drmModeGetPlaneResources failed\n");
		return -1;
	}

	info("drm: found planes %u", planes->count_planes);

	for (i = 0; i < planes->count_planes; ++i) {
		plane = drmModeGetPlane(fd, planes->planes[i]);
		if (!plane) {
			err("drmModeGetPlane failed: %s\n", strerror(errno));
			break;
		}
		
		info("drm: checking plane %u", plane->plane_id);

		if (!(plane->possible_crtcs & (1 << crtc_idx))) {
			drmModeFreePlane(plane);
			continue;
		}

		drm_get_plane_props(fd, plane->plane_id);

		drm_add_property(plane->plane_id, "FB_ID", 0);
		drm_add_property(plane->plane_id, "CRTC_ID", 0);
		drm_add_property(plane->plane_id, "SRC_X", 0);
		drm_add_property(plane->plane_id, "SRC_Y", 0);
		drm_add_property(plane->plane_id, "SRC_W", 0);
		drm_add_property(plane->plane_id, "SRC_H", 0);
		drm_add_property(plane->plane_id, "CRTC_X", 0);
		drm_add_property(plane->plane_id, "CRTC_Y", 0);
		drm_add_property(plane->plane_id, "CRTC_W", 0);
		drm_add_property(plane->plane_id, "CRTC_H", 0);
	
		info("drm: disabling plane %u", plane->plane_id);

		drmModeFreePlane(plane);

		drm_free_props();
	}

	drmModeFreePlaneResources(planes);

	return 0;
}

static struct drm_dev *drm_find_dev(int fd)
{
	int i;
	struct drm_dev *dev = NULL, *dev_head = NULL;
	drmModeRes *res;
	drmModeConnector *conn;
	drmModeEncoder *enc;
	drmModeCrtc *crtc = NULL;

	if ((res = drmModeGetResources(fd)) == NULL) {
		err("drmModeGetResources() failed");
		return NULL;
	}

	if (res->count_crtcs <= 0) {
		err("no Crtcs");
		goto free_res;
	}

	/* find all available connectors */
	for (i = 0; i < res->count_connectors; i++) {
		conn = drmModeGetConnector(fd, res->connectors[i]);

		if (conn) {
			if (conn->connection == DRM_MODE_CONNECTED) {
				dbg("drm: connector: connected");
			} else if (conn->connection == DRM_MODE_DISCONNECTED) {
				dbg("drm: connector: disconnected");
			} else if (conn->connection == DRM_MODE_UNKNOWNCONNECTION) {
				dbg("drm: connector: unknownconnection");
			} else {
				dbg("drm: connector: unknown");
			}
		}

		if (conn != NULL && conn->connection == DRM_MODE_CONNECTED
		    && conn->count_modes > 0) {
			dev = (struct drm_dev *) malloc(sizeof(struct drm_dev));
			memset(dev, 0, sizeof(struct drm_dev));

			dev->conn_id = conn->connector_id;
			dev->enc_id = conn->encoder_id;
			dev->next = NULL;

			if (conn->encoder_id) {
				enc = drmModeGetEncoder(fd, conn->encoder_id);
				if (!enc) {
					err("drmModeGetEncoder() faild");
					goto free_res;
				}
				if (enc->crtc_id) {
					crtc = drmModeGetCrtc(fd, enc->crtc_id);
					if (crtc) {
						dev->crtc_id = enc->crtc_id;
						drmModeFreeCrtc(crtc);
					}
				}
				drmModeFreeEncoder(enc);
			}

			dev->saved_crtc = NULL;

			/* create dev list */
			dev->next = dev_head;
			dev_head = dev;
		}
		drmModeFreeConnector(conn);
	}

	dev->crtc_idx = -1;

	for (i = 0; i < res->count_crtcs; ++i) {
		if (dev->crtc_id == res->crtcs[i]) {
			dev->crtc_idx = i;
			break;
		}
	}

	if (dev->crtc_idx == -1)
		err( "drm: CRTC not found\n");

free_res:
	drmModeFreeResources(res);

	return dev_head;
}

static int drm_open(const char *path)
{
	int fd, flags;

	fd = open(path, O_RDWR);
	if (fd < 0) {
		err("cannot open \"%s\"\n", path);
		return -1;
	}

	/* set FD_CLOEXEC flag */
	if ((flags = fcntl(fd, F_GETFD)) < 0 ||
	     fcntl(fd, F_SETFD, flags | FD_CLOEXEC) < 0) {
		err("fcntl FD_CLOEXEC failed\n");
		goto err;
	}

	return fd;
err:
	close(fd);
	return -1;
}

static int drm_init(const char *device)
{
	struct drm_dev *dev_head, *dev;
	drmModeAtomicReq *req ;
	int fd;
	int ret;

	fd = drm_open(device);
	if (fd < 0)
		return -1;

	ret = drmSetClientCap(fd, DRM_CLIENT_CAP_ATOMIC, 1);
	if (ret) {
		err("No atomic modesetting support: %s", strerror(errno));
		goto err;
	}

	dev_head = drm_find_dev(fd);
	if (dev_head == NULL) {
		err("available drm devices not found\n");
		goto err;
	}

	req = drmModeAtomicAlloc();

	dbg("available connector(s)");

	for (dev = dev_head; dev != NULL; dev = dev->next) {
		dbg("connector id:%d", dev->conn_id);
		dbg("\tencoder id:%d crtc id:%d", dev->enc_id,
		    dev->crtc_id);

		dev->fd = fd;
		dev->req = req;
		pdev = dev;

		ret = find_planes(fd, dev->crtc_id, dev->crtc_idx);
		if (ret) {
			err("Cannot find planes\n");
			goto err;
		}

		drm_get_crtc_props(fd, dev->crtc_id);

		drm_add_property(dev->crtc_id, "ACTIVE", 0);
		drm_add_property(dev->crtc_id, "MODE_ID", 0);
	
		info("drm: disabling crtc %u", dev->crtc_id);

		drm_free_props();
	}

	ret = drmModeAtomicCommit(fd, req,
			DRM_MODE_ATOMIC_ALLOW_MODESET, NULL);
	if (ret) {
		err("drmModeAtomicCommit failed: %s\n", strerror(errno));
		drmModeAtomicFree(req);
		goto err;
	}
	
	drmModeAtomicFree(req);

	return 0;

err:
	close(fd);
	pdev = NULL;
	return -1;
}

static const struct option options[] = {
	{
#define help_opt	0
		.name = "help",
		.has_arg = 0,
		.flag = NULL,
	},
	{
#define device_opt	5
		.name = "device",
		.has_arg = 1,
		.flag = NULL,
	},
	{
		.name = NULL,
	},
};

static void usage(void)
{
	fprintf(stderr, "usage: drm-blank <options>, with:\n");
	fprintf(stderr, "--help            display this menu\n");
	fprintf(stderr, "--device=<value>  dri device to use\n");
	fprintf(stderr, "\n");
}

int main(int argc, char *argv[])
{
	int ret;
	int lindex, opt;
	char *device_name = "/dev/dri/card0";

	for (;;) {
		lindex = -1;

		opt = getopt_long_only(argc, argv, "", options, &lindex);
		if (opt == EOF)
			break;

		switch (lindex) {
		case help_opt:
			usage();
			exit(0);
		case device_opt:
			device_name = optarg;
			break;
		default:
			usage();
			exit(1);
		}
	}

	/* initialize DRM with the format returned in the frame */
	ret = drm_init(device_name);
	if (ret) {
		err("Error initializing drm\n");
		exit(1);
	}

	sleep(10);

	return 0;
}
